var express = require('express');
var router = express.Router();
const cars = require("../controller/carController");
const auth = require("../middleware/auth");
const admins = require("../middleware/doubleAdmin");

router.get("/allcars",auth,cars.getCarsAvail);
router.post("/create",admins , cars.createCar);
router.put("/delete/:id",admins , cars.deleteCar);
router.put("/update/:id",admins , cars.updateCar);

module.exports = router;